# apg-analytics

Use maven to build and run the service locally.  Tomcat will bind to port 8080 by default for HTTP traffic.  The port for attaching jvm debugger is set to 17010.

If you don't want to install java or mvn, you can run the included docker images for building and running the service.

```
~/git/bitstew/apg-analytics (develop)$ ./build-images.sh
```

Once the images are built you can then compile the source codes by running appengine/build-analytics image

```
~/git/bitstew/apg-analytics (develop)$ ./build.sh
```

Then you can run the service in a docker container

```
~/git/bitstew/apg-analytics (develop)$ ./run.sh
```

Or if you already have mvn and familiar with java build environment just run `mvn clean package` followed by `mvn spring-boot:run`

```
~/git/bitstew/apg-analytics (develop)$ mvn spring-boot:run
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building apg-analytics CURRENT-SNAPSHOT
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] >>> spring-boot-maven-plugin:1.4.7.RELEASE:run (default-cli) > test-compile @ apg-analytics >>>
[INFO] 
[INFO] --- maven-resources-plugin:2.6:resources (default-resources) @ apg-analytics ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 0 resource
[INFO] Copying 0 resource
[INFO] 
[INFO] --- maven-compiler-plugin:3.1:compile (default-compile) @ apg-analytics ---
[INFO] Changes detected - recompiling the module!
[INFO] Compiling 6 source files to /Users/212580088/git/bitstew/apg-analytics/target/classes
[INFO] 
[INFO] --- maven-resources-plugin:2.6:testResources (default-testResources) @ apg-analytics ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 0 resource
[INFO] 
[INFO] --- maven-compiler-plugin:3.1:testCompile (default-testCompile) @ apg-analytics ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] <<< spring-boot-maven-plugin:1.4.7.RELEASE:run (default-cli) < test-compile @ apg-analytics <<<
[INFO] 
[INFO] --- spring-boot-maven-plugin:1.4.7.RELEASE:run (default-cli) @ apg-analytics ---
[INFO] Attaching agents: []
Listening for transport dt_socket at address: 17010

  .   ____          _            __ _ _
 /\\ / ___'_ __ _ _(_)_ __  __ _ \ \ \ \
( ( )\___ | '_ | '_| | '_ \/ _` | \ \ \ \
 \\/  ___)| |_)| | | | | || (_| |  ) ) ) )
  '  |____| .__|_| |_|_| |_\__, | / / / /
 =========|_|==============|___/=/_/_/_/
 :: Spring Boot ::        (v1.4.7.RELEASE)

2018-04-17 12:23:58.204  INFO 84070 --- [           main] a.a.AnalyticsExecutionServiceApplication : Starting AnalyticsExecutionServiceApplication on GC02RTAKCG8WPE with PID 84070 (/Users/212580088/git/bitstew/apg-analytics/target/classes started by 212580088 in /Users/212580088/git/bitstew/apg-analytics)
2018-04-17 12:23:58.207  INFO 84070 --- [           main] a.a.AnalyticsExecutionServiceApplication : The following profiles are active: secure-dummy,local
2018-04-17 12:23:58.258  INFO 84070 --- [           main] ationConfigEmbeddedWebApplicationContext : Refreshing org.springframework.boot.context.embedded.AnnotationConfigEmbeddedWebApplicationContext@30f842ca: startup date [Tue Apr 17 12:23:58 PDT 2018]; root of context hierarchy
2018-04-17 12:23:59.272  INFO 84070 --- [           main] s.b.c.e.t.TomcatEmbeddedServletContainer : Tomcat initialized with port(s): 8080 (http)
2018-04-17 12:23:59.282  INFO 84070 --- [           main] o.apache.catalina.core.StandardService   : Starting service [Tomcat]
2018-04-17 12:23:59.282  INFO 84070 --- [           main] org.apache.catalina.core.StandardEngine  : Starting Servlet Engine: Apache Tomcat/8.5.15
2018-04-17 12:23:59.351  INFO 84070 --- [ost-startStop-1] o.a.c.c.C.[Tomcat].[localhost].[/]       : Initializing Spring embedded WebApplicationContext
2018-04-17 12:23:59.352  INFO 84070 --- [ost-startStop-1] o.s.web.context.ContextLoader            : Root WebApplicationContext: initialization completed in 1096 ms
2018-04-17 12:23:59.458  INFO 84070 --- [ost-startStop-1] o.s.b.w.servlet.ServletRegistrationBean  : Mapping servlet: 'dispatcherServlet' to [/]
2018-04-17 12:23:59.461  INFO 84070 --- [ost-startStop-1] o.s.b.w.servlet.FilterRegistrationBean   : Mapping filter: 'characterEncodingFilter' to: [/*]
2018-04-17 12:23:59.461  INFO 84070 --- [ost-startStop-1] o.s.b.w.servlet.FilterRegistrationBean   : Mapping filter: 'hiddenHttpMethodFilter' to: [/*]
2018-04-17 12:23:59.461  INFO 84070 --- [ost-startStop-1] o.s.b.w.servlet.FilterRegistrationBean   : Mapping filter: 'httpPutFormContentFilter' to: [/*]
2018-04-17 12:23:59.461  INFO 84070 --- [ost-startStop-1] o.s.b.w.servlet.FilterRegistrationBean   : Mapping filter: 'requestContextFilter' to: [/*]
2018-04-17 12:23:59.658  INFO 84070 --- [           main] s.w.s.m.m.a.RequestMappingHandlerAdapter : Looking for @ControllerAdvice: org.springframework.boot.context.embedded.AnnotationConfigEmbeddedWebApplicationContext@30f842ca: startup date [Tue Apr 17 12:23:58 PDT 2018]; root of context hierarchy
2018-04-17 12:23:59.708  INFO 84070 --- [           main] s.w.s.m.m.a.RequestMappingHandlerMapping : Mapped "{[/v1/analytics/exec],methods=[POST],consumes=[application/json],produces=[application/json]}" onto public com.ge.apg.analytics.models.ExecutionResult com.ge.apg.analytics.controllers.ExecutionController.exec(com.ge.apg.analytics.models.Execution)
2018-04-17 12:23:59.709  INFO 84070 --- [           main] s.w.s.m.m.a.RequestMappingHandlerMapping : Mapped "{[/v1/analytics/ping],methods=[GET],produces=[text/plain]}" onto public java.lang.String com.ge.apg.analytics.controllers.ExecutionController.ping()
2018-04-17 12:23:59.710  INFO 84070 --- [           main] s.w.s.m.m.a.RequestMappingHandlerMapping : Mapped "{[/error]}" onto public org.springframework.http.ResponseEntity<java.util.Map<java.lang.String, java.lang.Object>> org.springframework.boot.autoconfigure.web.BasicErrorController.error(javax.servlet.http.HttpServletRequest)
2018-04-17 12:23:59.711  INFO 84070 --- [           main] s.w.s.m.m.a.RequestMappingHandlerMapping : Mapped "{[/error],produces=[text/html]}" onto public org.springframework.web.servlet.ModelAndView org.springframework.boot.autoconfigure.web.BasicErrorController.errorHtml(javax.servlet.http.HttpServletRequest,javax.servlet.http.HttpServletResponse)
2018-04-17 12:23:59.735  INFO 84070 --- [           main] o.s.w.s.handler.SimpleUrlHandlerMapping  : Mapped URL path [/webjars/**] onto handler of type [class org.springframework.web.servlet.resource.ResourceHttpRequestHandler]
2018-04-17 12:23:59.736  INFO 84070 --- [           main] o.s.w.s.handler.SimpleUrlHandlerMapping  : Mapped URL path [/**] onto handler of type [class org.springframework.web.servlet.resource.ResourceHttpRequestHandler]
2018-04-17 12:23:59.768  INFO 84070 --- [           main] o.s.w.s.handler.SimpleUrlHandlerMapping  : Mapped URL path [/**/favicon.ico] onto handler of type [class org.springframework.web.servlet.resource.ResourceHttpRequestHandler]
2018-04-17 12:23:59.926  INFO 84070 --- [           main] o.s.j.e.a.AnnotationMBeanExporter        : Registering beans for JMX exposure on startup
2018-04-17 12:23:59.979  INFO 84070 --- [           main] s.b.c.e.t.TomcatEmbeddedServletContainer : Tomcat started on port(s): 8080 (http)
2018-04-17 12:23:59.983  INFO 84070 --- [           main] a.a.AnalyticsExecutionServiceApplication : Started AnalyticsExecutionServiceApplication in 3.381 seconds (JVM running for 3.702)
```
