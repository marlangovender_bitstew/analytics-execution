#!/bin/bash

set -e

CURRENT_PATH="$( cd "$(dirname "$0")"; pwd -P)"
pushd ~/ > /dev/null
HOME_DIR="$( cd "$(dirname "$0")"; pwd -P)"
popd > /dev/null
docker run -v $CURRENT_PATH:/src -v $HOME_DIR/.m2:/root/.m2 --rm appengine/build-analytics