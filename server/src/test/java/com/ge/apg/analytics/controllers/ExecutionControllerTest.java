/*
 * Copyright (c) 2018 General Electric Company. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * General Electric Company. The software may be used and/or copied only
 * with the written permission of General Electric Company or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 */
package com.ge.apg.analytics.controllers;

import static com.ge.apg.docker.TaskResultStatus.SUCCESS;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.WebApplicationContext;

import com.ge.apg.docker.DockerClientException;
import com.ge.apg.docker.DockerRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ExecutionControllerTest {

    @TestConfiguration
    static class TestConfig {
        @Bean
        public DockerRunner getDockerRunner() {
            return Mockito.mock(DockerRunner.class);
        }


        @Bean
        public RestTemplate getRestTemplate(RestTemplateBuilder builder) {
            return builder.build();
        }
    }

    private static final Path BASE_TEST_DIR = Paths.get(System.getProperty("java.io.tmpdir"),"ect");

    @Autowired
    private WebApplicationContext wac;

    private MockMvc mockMvc;

    @Autowired
    private DockerRunner runner;

    @Autowired
    private ExecutionController executionController;

    @Before
    public void setup() throws Exception {
        ReflectionTestUtils.setField(executionController, "inputOutputDir", BASE_TEST_DIR.toString());
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
    }

    @After
    public void cleanupTestVars() throws Exception {
        FileUtils.deleteDirectory(BASE_TEST_DIR.toFile());
    }

    @Test
    public void testHealth() throws Exception {
        mockMvc.perform(get(ExecutionController.API_BASE + ExecutionController.API_HEALTH))
                .andExpect(status().is(HttpStatus.OK.value()));
    }

    @Test
    public void testExec_happyPath() throws Exception {
        when(runner.runTask(any())).thenReturn(SUCCESS);
        mockMvc.perform(post(ExecutionController.API_BASE + "/exec/container/dummy_container"))
               .andExpect(status().is(HttpStatus.OK.value()));
    }

    @Test
    public void exec_BadLocation_Failure() throws Exception {
        mockMvc.perform(post(ExecutionController.API_BASE + "/exec/container/dummy_container"))
               .andExpect(status().is(HttpStatus.INTERNAL_SERVER_ERROR.value()));
    }

    @Test
    public void testExec_Failure() throws Exception {
        when(runner.runTask(any())).thenThrow(DockerClientException.class);
        mockMvc.perform(post(ExecutionController.API_BASE + "/exec/location/bad"))
                .andExpect(status().is(HttpStatus.INTERNAL_SERVER_ERROR.value()));
    }

    @Test
    public void readInputs_NoFiles_EmptyMap() throws Exception {
        Path testDir = Paths.get(BASE_TEST_DIR.toString(), "emptyFolderScenario",ExecutionController.INPUT_DIR);
        testDir.toFile().mkdirs();
        Map<String, List<String[]>> result = executionController.readInputs("emptyFolderScenario");

        assertNotNull(result);
        assertEquals(0, result.size());
    }

    @Test
    public void readInputs_EmptyFile_MapWithEmptyList() throws Exception {
        Path testDir = Paths.get(BASE_TEST_DIR.toString(), "emptyFileScenario", ExecutionController.INPUT_DIR);
        testDir.toFile().mkdirs();
        Path emptyCsv = Paths.get(testDir.toString(), "file1.csv");
        FileUtils.touch(emptyCsv.toFile());
        Map<String, List<String[]>> result = executionController.readInputs("emptyFileScenario");
        assertNotNull(result);
        assertEquals(1, result.size());
        assertEquals(0, result.values().iterator().next().size());
    }

    @Test
    public void readInputs_CSVWithEmptyLines_MapWithoutEmptyLines() throws Exception {
        Path testDir = Paths.get(BASE_TEST_DIR.toString(), "emptyLinesScenario", ExecutionController.INPUT_DIR);
        testDir.toFile().mkdirs();
        Path emptyCsv = Paths.get(testDir.toString(), "fileWithEmpty.csv");
        FileUtils.touch(emptyCsv.toFile());
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(emptyCsv.toFile())))  {
            bw.write("\"0\", \"1\", \"2\", \"3\"");
            bw.newLine();
            bw.write("\"1\", \"2\", \"3\", \"4\"");
        }

        Map<String, List<String[]>> result = executionController.readInputs("emptyLinesScenario");
        assertNotNull(result);
        assertEquals(1, result.size());
        assertArrayEquals(new String[]{"0","1","2","3"}, result.get("fileWithEmpty.csv").get(0));
        assertArrayEquals(new String[]{"1","2","3","4"}, result.get("fileWithEmpty.csv").get(1));
    }

    @Test
    public void readInputs_CSVWithTrainingNewLine_MapWithoutTrailingEntry() throws Exception {
        Path testDir = Paths.get(BASE_TEST_DIR.toString(), "trailingLineScenario", ExecutionController.INPUT_DIR);
        testDir.toFile().mkdirs();
        Path emptyCsv = Paths.get(testDir.toString(), "fileWithEmpty.csv");
        FileUtils.touch(emptyCsv.toFile());
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(emptyCsv.toFile())))  {
            bw.write("\"0\", \"1\", \"2\", \"3\"");
            bw.newLine();
            bw.write("\"1\", \"2\", \"3\", \"4\"");
            bw.newLine();
        }

        Map<String, List<String[]>> result = executionController.readInputs("trailingLineScenario");
        assertNotNull(result);
        assertEquals(1, result.size());
        assertArrayEquals(new String[]{"0","1","2","3"}, result.get("fileWithEmpty.csv").get(0));
        assertArrayEquals(new String[]{"1","2","3","4"}, result.get("fileWithEmpty.csv").get(1));
    }
}
