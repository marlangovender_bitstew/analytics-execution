package com.ge.apg.analytics;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.equalTo;
import static org.junit.Assert.assertEquals;

import javax.ws.rs.core.MediaType;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.ge.apg.analytics.config.TenantConfig;
import com.ge.apg.analytics.config.TenantType;
import com.ge.apg.analytics.config.UaaConfig;
import com.github.tomakehurst.wiremock.client.WireMock;
import com.github.tomakehurst.wiremock.junit.WireMockRule;

/**
 * Created by Tim Tan 212675067
 */

@RunWith(SpringJUnit4ClassRunner.class)
@EnableConfigurationProperties({ UaaConfig.class, TenantConfig.class })
@ContextConfiguration(classes = TenantContextConfig.class)
public class UaaGetterServiceTest {

    private UaaGetterService uaaGetterService;
    private static int port = 18081;

    @Rule
    public WireMockRule wireMockRule = new WireMockRule(port);

    @Autowired
    private UaaConfig uaaConfig;

    @Autowired
    private TenantConfig tenantConfig;

    @Before
    public void setUp() throws Exception {
        uaaGetterService = new UaaGetterService(uaaConfig, tenantConfig);
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void getAccessTokenForTenant_ValidRequest_ReturnsValidAccessToken() throws Exception {
        String resultBody = "{\"access_token\": \"bearer testAccessToken\",\n"
                + "    \"token_type\": \"bearer\",\n" + "    \"expires_in\": 43199,\n"
                + "    \"scope\": \"testScope\",\n" + "    \"jti\": \"testJti\"\n}";

        WireMock.stubFor(WireMock.post(WireMock.urlEqualTo("/oauth/token"))
                .withHeader(HttpHeaders.CONTENT_TYPE,
                        equalTo(MediaType.APPLICATION_FORM_URLENCODED))
                .withHeader(HttpHeaders.AUTHORIZATION,
                        equalTo("Basic " + uaaGetterService.getEncodedUaaCredentials()))
                .withRequestBody(equalTo("grant_type=client_credentials"))
                .willReturn(aResponse().withStatus(200)
                        .withHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                        .withBody(resultBody)));

        UaaAccessToken result = uaaGetterService
                .getAccessTokenForTenant(TenantType.PREDIX_ANALYTICS_FRAMEWORK);
        assertEquals(result.getTenantId(), tenantConfig.getPredixAnalyticsFramework());
        assertEquals(result.getBearerToken(), "bearer testAccessToken");
    }

    @Test(expected = RuntimeException.class)
    public void getAccessTokenForTenant_InvalidRequest_ThrowsRuntimeException() throws Exception {
        String resultBody = "Unexpected failure";
        WireMock.stubFor(WireMock.post(WireMock.urlEqualTo("/oauth/token"))
                .willReturn(aResponse().withStatus(401)
                        .withHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                        .withBody(resultBody)));

        uaaGetterService.getAccessTokenForTenant(TenantType.PREDIX_ANALYTICS_FRAMEWORK);
    }
}
