/*
 * Copyright (c) 2018 General Electric Company. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * General Electric Company. The software may be used and/or copied only
 * with the written permission of General Electric Company or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 */
package com.ge.apg.analytics.models;

import java.util.Map;

public class Execution {
    private String techniqueId;
    private String modelId;
    private Map<Object,Object> parameters;
    private String inputDataFrameId;
    private String outputDataFrameId;
    private String callbackUrl;

    public String getTechniqueId() {
        return techniqueId;
    }

    public void setTechniqueId(String techniqueId) {
        this.techniqueId = techniqueId;
    }

    public String getModelId() {
        return modelId;
    }

    public void setModelId(String modelId) {
        this.modelId = modelId;
    }

    public Map<Object, Object> getParameters() {
        return parameters;
    }

    public void setParameters(Map<Object, Object> parameters) {
        this.parameters = parameters;
    }

    public String getInputDataFrameId() {
        return inputDataFrameId;
    }

    public void setInputDataFrameId(String inputDataFrameId) {
        this.inputDataFrameId = inputDataFrameId;
    }

    public String getOutputDataFrameId() {
        return outputDataFrameId;
    }

    public void setOutputDataFrameId(String outputDataFrameId) {
        this.outputDataFrameId = outputDataFrameId;
    }

    public String getCallbackUrl() {
        return callbackUrl;
    }

    public void setCallbackUrl(String callbackUrl) {
        this.callbackUrl = callbackUrl;
    }
}
