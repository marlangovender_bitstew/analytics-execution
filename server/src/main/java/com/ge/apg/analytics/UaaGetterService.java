package com.ge.apg.analytics;

import com.ge.apg.analytics.config.TenantConfig;
import com.ge.apg.analytics.config.TenantType;
import com.ge.apg.analytics.config.UaaConfig;
import com.google.common.annotations.VisibleForTesting;
import java.util.Base64;
import java.util.Map;
import javax.ws.rs.core.MediaType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

/**
 * Created by Tim Tan 212675067
 */
@Component
public class UaaGetterService {
    private static final String AUTH_PREFIX = "Basic ";

    private static final String AUTH_GRANT_TYPE_KEY = "grant_type";
    private static final String AUTH_GRANT_TYPE = "client_credentials";

    private UaaConfig uaaConfig;
    private TenantConfig tenantConfig;

    @Autowired
    public UaaGetterService(UaaConfig uaaConfig, TenantConfig tenantConfig) {
        this.uaaConfig = uaaConfig;
        this.tenantConfig = tenantConfig;
    }

    /**
     * Provides a mechanism for retrieving UAA tokens for multiple tenants on a single UAA.
     * 
     * @param tenantType
     *            The type of tenant to retrieve an access token for.
     * @return A UaaAccessToken containing the tenantId/Bearer for the specified tenantType.
     */
    public UaaAccessToken getAccessTokenForTenant(TenantType tenantType) throws Exception
    {
        String base64EncodedUaaCredentials = getEncodedUaaCredentials();
        String uaaUrl = uaaConfig.getServiceEndpoint();

        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add(HttpHeaders.AUTHORIZATION, AUTH_PREFIX + base64EncodedUaaCredentials);
        headers.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_FORM_URLENCODED);

        MultiValueMap<String, String> postBody = new LinkedMultiValueMap<>();
        postBody.add(AUTH_GRANT_TYPE_KEY, AUTH_GRANT_TYPE);

        RestTemplate rt = new RestTemplate();

        ParameterizedTypeReference<Map<String, String>> responseTypeRef = new ParameterizedTypeReference<Map<String, String>>() {
        };

        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(postBody, headers);
        ResponseEntity<Map<String, String>> response = rt.exchange(uaaUrl, HttpMethod.POST, request,
                responseTypeRef);
        if (response.getStatusCode() != HttpStatus.OK) {
            throw new Exception("Could not retrieve access token for " + tenantType
                    + ". Error:" + response.getBody());
        }

        Map<String, String> responseBody = response.getBody();

        return new UaaAccessToken(tenantConfig.getTenantForType(tenantType),
                responseBody.get("access_token"));
    }

    @VisibleForTesting
    String getEncodedUaaCredentials() {
        String uaaUser = uaaConfig.getUser();
        String uaaPassword = uaaConfig.getPassword();
        byte[] encoded = Base64.getEncoder().encode((uaaUser + ":" + uaaPassword).getBytes());

        return new String(encoded);
    }
}
