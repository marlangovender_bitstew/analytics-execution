package com.ge.apg.analytics;

import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.ge.apg.docker.DockerRunner;
import com.spotify.docker.client.DefaultDockerClient;
import com.spotify.docker.client.exceptions.DockerCertificateException;
import org.springframework.web.client.RestTemplate;

@Configuration
public class DefaultConfiguration {

    @Bean
    public DockerRunner getDockerRunner() throws DockerCertificateException {
        return new DockerRunner(DefaultDockerClient.fromEnv().build());
    }

    @Bean
    public RestTemplate getRestTemplate(RestTemplateBuilder builder) {
        return builder.build();
    }
}
