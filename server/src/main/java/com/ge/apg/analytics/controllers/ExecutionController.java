/*
 * Copyright (c) 2018 General Electric Company. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * General Electric Company. The software may be used and/or copied only
 * with the written permission of General Electric Company or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 */
package com.ge.apg.analytics.controllers;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ge.apg.analytics.UaaAccessToken;
import com.ge.apg.analytics.UaaGetterService;
import com.ge.apg.analytics.config.TenantType;
import com.ge.apg.analytics.paf.PredixAnalyticsFrameworkClient;
import com.ge.apg.docker.DockerClientException;
import com.ge.apg.docker.DockerRunner;
import com.ge.apg.docker.TaskResultStatus;
import com.ge.apg.docker.TaskRunOptions;
import com.google.common.annotations.VisibleForTesting;
import com.opencsv.CSVReader;
import com.spotify.docker.client.exceptions.DockerException;

@RestController
@RequestMapping(ExecutionController.API_BASE)
public class ExecutionController {
    private static final Logger logger = LoggerFactory.getLogger("ExecutionController");

    public static final String API_BASE = "/v1/analytics";
    public static final String API_HEALTH = "/health";
    public static final String API_EXEC = "/exec/{location}/{id}";

    private static final String IMAGE_PREFIX = "appengine";

    @VisibleForTesting
    static final String INPUT_DIR = "input";
    @VisibleForTesting
    static final String OUTPUT_DIR = "output";

    private static final String IMAGE_TAG_LATEST = "latest";

    @Value("${apg.analytics.input-output-dir}")
    private String inputOutputDir;

    @Value("${paf.executionEndpoint}")
    private String executionEndpoint;

    private final DockerRunner runner;
    private final UaaGetterService uaaGetterService;
    private final PredixAnalyticsFrameworkClient pafClient;

    @Autowired
    public ExecutionController(DockerRunner runner, UaaGetterService uaaGetterService, PredixAnalyticsFrameworkClient pafClient) {
        this.runner = runner;
        this.uaaGetterService = uaaGetterService;
        this.pafClient = pafClient;
    }

    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    private String handleGenericException(Exception exception) {
        logger.error("Uncaught Exception.", exception);
        return "Unhandled Exception: " + exception.getMessage();
    }

    @RequestMapping(method = RequestMethod.GET, value = API_HEALTH, produces = MediaType.TEXT_PLAIN_VALUE)
    public String health() {
        logger.info("health check");
        return "ok";
    }

    private String getImageName(String id) {
        return IMAGE_PREFIX + "/" + id;
    }

    private String getSharedPath(String id) {
        return inputOutputDir + File.separator + id;
    }

    private String getInputDir(String id) {
        return getSharedPath(id) + File.separator + INPUT_DIR;
    }

    private String getOutputDir(String id) {
        return getSharedPath(id) + File.separator + OUTPUT_DIR;
    }

    @RequestMapping(method = RequestMethod.POST, value = API_EXEC, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public Map<String, Object> execute(@PathVariable String location, @PathVariable String id)
            throws Exception {
        Map<String, Object> returnMap;
        switch (location) {
            case "container":
                returnMap = handleContainerExecution(id);
                break;
            case "predixAnalyticsFramework":
                returnMap = handlePredixApplicationFrameworkExecution(id);
                break;
            default:
                throw new IllegalArgumentException("Unhandled location type :" + location);
        }
        return returnMap;
    }

    private Map<String, Object> handlePredixApplicationFrameworkExecution(String id)
            throws Exception {
        UaaAccessToken accessToken = uaaGetterService
                .getAccessTokenForTenant(TenantType.PREDIX_ANALYTICS_FRAMEWORK);

        Map<String, List<String[]>> inputs = readInputs(id);
        Map<String, Map<String, Map<String, Object>>> csvInJsonTransferRepresentation = convertToJsonRepresentation(inputs);
        ObjectMapper mapper = new ObjectMapper();
        String response = pafClient.executeSync(executionEndpoint, accessToken.getTenantId(), accessToken.getBearerToken(), mapper.writeValueAsString(csvInJsonTransferRepresentation.values().iterator().next()));

        System.out.println(csvInJsonTransferRepresentation);
        System.out.println(response);

        writeOutput(id, response);
        HashMap<String,Object> result = new HashMap<>();
        result.put("output", response);
        return result;
    }

    /**
     * Converts multiple CSV documents to their Json transfer representation.
     * @param inputs The CSV documents to convert
     * @return A Map representing the resultant CSV documents
     */
    private Map<String, Map<String, Map<String, Object>>> convertToJsonRepresentation(Map<String, List<String[]>> inputs) {
        Map<String, Map<String, Map<String, Object>>> result = new LinkedHashMap<>();

        for (Map.Entry<String, List<String[]>> input : inputs.entrySet()) {
            result.put(input.getKey(),convertToJsonRepresentation(input.getValue()));
        }
        return result;
    }

    /**
     * Converts a CSV document in List<String[]> form -> their Json transfer representation with default headers as row[x], col[y]
     * Representation as follows:
     * {
     *     "row1":
     *     {
     *         "col1":val1,
     *         "col2":val2,
     *         "col3":val3
     *     },
     *     "row2":
     *     {...
     * }
     * @param input A CSV document represented in List<String[]> format
     * @return
     */
    private Map<String, Map<String, Object>> convertToJsonRepresentation( List<String[]> input) {
        int row = 1;
        int col = 1;

        Map<String, Map<String, Object>> result = new LinkedHashMap<>();
        for (String[] inputRow : input) {
            String rowKey = "row" + row++;
            Map<String, Object> rowContents = new LinkedHashMap<>();
            for(String entry : inputRow) {
                //TODO: more sophisticated conversion logic. Currently everything is expected to be a Double.
                Object parsedEntry = entry;
                try {
                    parsedEntry = Double.parseDouble(entry);
                } catch (NumberFormatException e) {
                    // continue
                }
                rowContents.put("col" + col++, parsedEntry);
            }
            col = 1;
            result.put(rowKey, rowContents);
        }
        return result;
    }

    /**
     * Reads csv files in the input directory. Does not follow symLinks.
     * @param analyticId
     * @return A List<List<String[]>> containing the contents of csvs. Each inner list represents a file, with each String[]
     *         representing a row.
     * @throws IOException
     */
    @VisibleForTesting
    Map<String, List<String[]>> readInputs(String analyticId) throws IOException {
        Path inputDirectory = Paths.get(getInputDir(analyticId));
        try (Stream<Path> paths = Files.walk(inputDirectory, 1)) {
            List<Path> inputCSVFiles = paths
                      .filter(Files::isRegularFile)
                      .filter(path -> path.toString().toLowerCase().endsWith(".csv"))
                      .collect(Collectors.toList());

            Map<String, List<String[]>> csvs = new HashMap<>();
            for (Path file : inputCSVFiles) {
                FileReader reader = new FileReader(file.toFile());
                BufferedReader bufferedReader = new BufferedReader(reader);
                CSVReader csvReader = new CSVReader(bufferedReader);
                csvs.put(file.getFileName().toString(), filterCSV(csvReader));
            }
            return csvs;
        }
    }

    private List<String[]> filterCSV(CSVReader csvReader) throws IOException {
        List<String[]> output = new ArrayList<>();
        try {
            csvReader.iterator().forEachRemaining( line -> {
                if (line.length == 1 && Objects.equals(line[0], "")) {
                    //skip empty line
                }
                else {
                    output.add(line);
                }
            });
        } finally {
            csvReader.close();
        }
        return output;
    }

    private void writeOutput(String analyticId, String outputDataFrame) throws IOException {
        File outputFile = new File(getOutputDir(analyticId) + File.separator + "output.csv");
        try (FileOutputStream outputStream = new FileOutputStream(outputFile)) {
            IOUtils.write(outputDataFrame, outputStream, "UTF-8");
        }
    }

    private Map<String, Object> handleContainerExecution(String id)
            throws InterruptedException, DockerException, DockerClientException {
        Map<String, Object> returnMap = new HashMap<>();
        TaskRunOptions options = new TaskRunOptions(getImageName(id), IMAGE_TAG_LATEST,
                getInputDir(id), getOutputDir(id));
        TaskResultStatus result = runner.runTask(options);
        returnMap.put("Result", result.toString());
        return returnMap;
    }
}
