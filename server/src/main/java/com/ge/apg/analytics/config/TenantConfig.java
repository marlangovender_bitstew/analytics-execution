package com.ge.apg.analytics.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * Created by Tim Tan 212675067
 */
@Component
@EnableConfigurationProperties
@ConfigurationProperties(prefix = "tenant")
public class TenantConfig {
    private String predixAnalyticsFramework;
    private String predixTimeseries;

    public TenantConfig() {
    }

    public String getTenantForType(TenantType type) {
        if (type == null) {
            throw new RuntimeException("TenantConfig#getTenantForType: type can not be null");
        }
        switch (type) {
            case PREDIX_ANALYTICS_FRAMEWORK:
                return getPredixAnalyticsFramework();
            case PREDIX_TIMESERIES:
                return getPredixTimeseries();
            default:
                throw new RuntimeException(
                        "TenantConfig#getTenantForType: unhandled type [" + type + "]");
        }
    }

    public String getPredixAnalyticsFramework() {
        return predixAnalyticsFramework;
    }

    public void setPredixAnalyticsFramework(String predixAnalyticsFramework) {
        this.predixAnalyticsFramework = predixAnalyticsFramework;
    }

    public String getPredixTimeseries() {
        return predixTimeseries;
    }

    public void setPredixTimeseries(String predixTimeseries) {
        this.predixTimeseries = predixTimeseries;
    }
}
