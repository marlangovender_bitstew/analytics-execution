package com.ge.apg.analytics;

/**
 * Created by Tim Tan 212675067
 */
public class UaaAccessToken {
    private String tenantId;
    private String bearerToken;

    public UaaAccessToken(String tenantId, String bearerToken) {
        this.tenantId = tenantId;
        this.bearerToken = bearerToken;
    }

    public String getTenantId() {
        return tenantId;
    }

    public void setTenantId(String tenantId) {
        this.tenantId = tenantId;
    }

    public String getBearerToken() {
        return bearerToken;
    }

    public void setBearerToken(String bearerToken) {
        this.bearerToken = bearerToken;
    }
}
