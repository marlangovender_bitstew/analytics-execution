package com.ge.apg.analytics.config;

/**
 * Created by Tim Tan 212675067
 */
public enum TenantType {
    PREDIX_ANALYTICS_FRAMEWORK,
    PREDIX_TIMESERIES
}
