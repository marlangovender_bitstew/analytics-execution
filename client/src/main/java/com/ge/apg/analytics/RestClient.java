/*
 * Copyright (c) 2018 General Electric Company. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * General Electric Company. The software may be used and/or copied only
 * with the written permission of General Electric Company or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 */
package com.ge.apg.analytics;

import com.fasterxml.jackson.core.type.TypeReference;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.http.HttpEntity;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.entity.ContentType;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import com.fasterxml.jackson.databind.ObjectMapper;

public class RestClient {

    public static final String CHARSET_UTF8 = "UTF-8";
    public static final String ANALYTIC_ID = "analyticId";

    public static final String OUTPUT = "output";
    private static final String LOCATION = "location";

    private final ObjectMapper objectMapper;

    private String endpointUrl;

    private static String ENDPOINT_HEALTH = "/v1/analytics/health";
    private static String ENDPOINT_EXEC = "/v1/analytics/exec";

    private static String HEALTH_OK = "ok";

    public RestClient(String endpointUrl) {
        this.objectMapper = new ObjectMapper();
        this.endpointUrl = endpointUrl;
    }

    public String getEndpointUrl() {
        return endpointUrl;
    }

    private String getHealthEndpoint() {
        return endpointUrl + ENDPOINT_HEALTH;
    }

    private String getExecutionEndpoint(String location, String analyticId) {
        return endpointUrl + ENDPOINT_EXEC + '/' + location + '/' + analyticId;
    }

    public void healthCheck() throws IOException, ClientException {
        CloseableHttpClient httpclient = HttpClients.createDefault();
        try {
            HttpGet method = new HttpGet(getHealthEndpoint());
            CloseableHttpResponse response = httpclient.execute(method);
            int status = response.getStatusLine().getStatusCode();
            if (status != HttpStatus.SC_OK) {
                throw ClientException.HEALTH_CHECK(response.getStatusLine().toString());
            }
            String body = EntityUtils.toString(response.getEntity());
            if (!HEALTH_OK.equalsIgnoreCase(body)) {
                throw ClientException.HEALTH_CHECK(body);
            }
        } finally {
            httpclient.close();
        }
    }

    public Map<String, Object> execute(String location, String analyticId)
            throws ClientException, IOException {
        CloseableHttpClient httpclient = HttpClients.createDefault();
        try {
            HttpPost method = new HttpPost(getExecutionEndpoint(location, analyticId));
            CloseableHttpResponse response = httpclient.execute(method);
            int status = response.getStatusLine().getStatusCode();
            if (status != HttpStatus.SC_OK) {
                throw ClientException.EXECUTION_ERROR(response.getStatusLine().toString());
            }
            String body = EntityUtils.toString(response.getEntity());
            return objectMapper.readValue(body, new TypeReference<Map<String, Object>>() {
            });
        } finally {
            httpclient.close();
        }
    }
}
