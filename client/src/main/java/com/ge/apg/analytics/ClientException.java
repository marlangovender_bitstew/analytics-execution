/*
 * Copyright (c) 2018 General Electric Company. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * General Electric Company. The software may be used and/or copied only
 * with the written permission of General Electric Company or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 */
package com.ge.apg.analytics;

public class ClientException extends Exception {
    public static final String CODE_HEALTH_ERROR = "healthcheck";
    public static final String CODE_EXECUTION_ERROR = "execution";

    public static ClientException HEALTH_CHECK() {
        return HEALTH_CHECK(null);
    }

    public static ClientException HEALTH_CHECK(String message) {
        return new ClientException(CODE_HEALTH_ERROR, message);
    }

    public static ClientException EXECUTION_ERROR(String message) {
        return new ClientException(CODE_EXECUTION_ERROR, message);
    }

    private String code;

    public ClientException(String code, String message) {
        super(message);
        this.code = code;
    }

    public String getCode() {
        return code;
    }
}
