/*
 * Copyright (c) 2018 General Electric Company. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * General Electric Company. The software may be used and/or copied only
 * with the written permission of General Electric Company or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 */
package com.ge.apg.analytics;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.exactly;
import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.client.WireMock.getRequestedFor;
import static com.github.tomakehurst.wiremock.client.WireMock.post;
import static com.github.tomakehurst.wiremock.client.WireMock.postRequestedFor;
import static com.github.tomakehurst.wiremock.client.WireMock.stubFor;
import static com.github.tomakehurst.wiremock.client.WireMock.urlEqualTo;
import static com.github.tomakehurst.wiremock.client.WireMock.urlMatching;
import static com.github.tomakehurst.wiremock.client.WireMock.verify;

import java.util.Map;

import org.junit.Assert;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import com.github.tomakehurst.wiremock.client.WireMock;
import com.github.tomakehurst.wiremock.junit.WireMockClassRule;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(loader = AnnotationConfigContextLoader.class)
public class RestClientTest {

    @Configuration
    static class TestConfig {
        @Bean
        public RestClient restClient() {
            return new RestClient(endpoint);
        }
    }

    @Autowired
    private RestClient restClient;

    private static String endpoint = "http://localhost:18080";
    private static int port = 18080;

    @ClassRule
    public static WireMockClassRule wireMockClassRule = new WireMockClassRule(port);

    @Rule
    public WireMockClassRule wireMockRule = wireMockClassRule;

    private static final String healthEndpoint = "/v1/analytics/health";
    private static final String execEndpoint = "/v1/analytics/exec";

    private static final String location = "location";
    private static final String analyticId = "analyticId";

    @Before
    public void setup() throws Exception {

    }

    @Test
    public void testBeanCreation() {
        Assert.assertNotNull(restClient);
        Assert.assertEquals(endpoint, restClient.getEndpointUrl());
    }

    @Test
    public void testHealth() throws Exception {
        WireMock.reset();
        stubFor(get(urlEqualTo(healthEndpoint))
                .willReturn(aResponse().withStatus(200).withBody("ok")));
        restClient.healthCheck();
        verify(exactly(1), getRequestedFor(urlEqualTo(healthEndpoint)));
    }

    @Test(expected = ClientException.class)
    public void testHealthError() throws Exception {
        WireMock.reset();
        stubFor(get(urlEqualTo(healthEndpoint))
                .willReturn(aResponse().withStatus(500).withBody("Internal Server Error")));
        restClient.healthCheck();
    }

    @Test
    public void testExec() throws Exception {
        WireMock.reset();
        stubFor(post(urlMatching(execEndpoint + "/.*")).willReturn(aResponse().withStatus(200)
                .withHeader("Content-Type", "text/plain").withBody("{\"output\":\"abcde\"}")));
        Map result = restClient.execute(location, analyticId);
        verify(exactly(1), postRequestedFor(urlMatching(execEndpoint + "/" + location + "/" + analyticId)));
        Assert.assertEquals("output is not same", "abcde", result.get("output"));
    }

    @Test(expected = ClientException.class)
    public void testExecHttpError() throws Exception {
        WireMock.reset();
        stubFor(post(urlEqualTo(execEndpoint)).willReturn(aResponse().withStatus(500)));
        restClient.execute(location, analyticId);
    }

    @Test(expected = ClientException.class)
    public void testExecResponseError() throws Exception {
        WireMock.reset();
        stubFor(post(urlEqualTo(execEndpoint)).willReturn(aResponse().withStatus(200)
                .withHeader("Content-Type", "text/plain").withBody("{\"some-attr\":\"abcde\"}")));
        restClient.execute(location, analyticId);
    }
}
