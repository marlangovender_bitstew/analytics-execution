package com.ge.apg.docker;

/**
 * Allows for the propagation of checked exceptions without explicit declaration
 *
 * Created by Tim Tan 212675067
 *
 */
public class SneakyThrow {
    @SuppressWarnings("unchecked")
    public static <E extends Throwable> void wrap(Throwable e) throws E {
        throw (E) e;
    }

    public static <R, E extends Throwable> R uncheck(ThrowingCommand<R, E> cmd) {
        try {
            return cmd.run();
        } catch (Throwable e) {
            wrap(e);
        }
        throw new RuntimeException("Unexpected failure with SneakyThrow::uncheck");
    }

    public static <E extends Throwable> void uncheck(ThrowingRunnable<E> runnable) {
        try {
            runnable.run();
        } catch (Throwable e) {
            wrap(e);
        }
    }
}
