package com.ge.apg.docker;

/**
 * Created by Tim Tan 212675067
 */
@FunctionalInterface
public interface ThrowingRunnable<E extends Throwable> {
    void run() throws E;
}
