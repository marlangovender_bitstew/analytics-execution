package com.ge.apg.docker;

import java.util.Collection;
import java.util.Optional;
import java.util.stream.Stream;

/**
 * Created by Tim Tan 212675067
 */
public class StreamUtils {
    public static <T> Stream<T> emptyIfNull(Collection<T> input) {
        return Optional.ofNullable(input).map(Collection::stream).orElse(Stream.empty());
    }
}
