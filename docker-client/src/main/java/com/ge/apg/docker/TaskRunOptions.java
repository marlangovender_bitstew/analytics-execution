/*
 * Copyright (c) 2018 General Electric Company. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * General Electric Company. The software may be used and/or copied only
 * with the written permission of General Electric Company or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 */
package com.ge.apg.docker;

/**
 * Created by Tim Tan 212675067
 */
public class TaskRunOptions {
    private String imageRepository;
    private String imageTag;
    private String inputFilePath;
    private String outputFilePath;

    public TaskRunOptions(String imageRepository, String imageTag, String inputFilePath, String outputFilePath) {
        this.imageRepository = imageRepository;
        this.imageTag = imageTag;
        this.inputFilePath = inputFilePath;
        this.outputFilePath = outputFilePath;
    }

    public String getInputFilePath() {
        return inputFilePath;
    }

    public String getOutputFilePath() {
        return outputFilePath;
    }

    public String getImageRepository() {
        return imageRepository;
    }

    public String getImageTag() {
        return imageTag;
    }

    public String toString() {
        return "Image: " + imageRepository + ":" + imageTag + ", input:" + inputFilePath + ", output:" + outputFilePath;
    }
}
