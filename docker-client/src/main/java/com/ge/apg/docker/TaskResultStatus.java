package com.ge.apg.docker;

/**
 * Created by Tim Tan 212675067
 */
public enum TaskResultStatus {
    SUCCESS, FAILURE
}
