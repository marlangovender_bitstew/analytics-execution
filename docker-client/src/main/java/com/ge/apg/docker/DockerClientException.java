/*
 * Copyright (c) 2018 General Electric Company. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * General Electric Company. The software may be used and/or copied only
 * with the written permission of General Electric Company or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 */
package com.ge.apg.docker;

public class DockerClientException extends Exception {

    public static DockerClientException IMAGE_NOT_FOUND(String imageRepository, String imageTag) {
        return new DockerClientException(imageRepository + ":" + imageTag + " is not found");
    }

    public static DockerClientException TOO_MANY_IMAGES(String imageRepository, String imageTag) {
        return new DockerClientException("Too many images found for " + imageRepository + ":" + imageTag);
    }
    public DockerClientException(String message) {
        super(message);
    }
}
