package com.ge.apg.docker;

/**
 * Created by Tim Tan 212675067
 */
@FunctionalInterface
public interface ThrowingCommand<R, E extends Throwable> {
    R run() throws E;
}
