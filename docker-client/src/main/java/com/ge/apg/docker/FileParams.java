package com.ge.apg.docker;

/**
 * Created by Tim Tan 212675067
 */
public class FileParams {
    private String inputFilePath;
    private String outputFilePath;

    public FileParams() {
    }

    public String getInputFilePath() {
        return inputFilePath;
    }

    public void setInputFilePath(String inputFilePath) {
        this.inputFilePath = inputFilePath;
    }

    public String getOutputFilePath() {
        return outputFilePath;
    }

    public void setOutputFilePath(String outputFilePath) {
        this.outputFilePath = outputFilePath;
    }
}
