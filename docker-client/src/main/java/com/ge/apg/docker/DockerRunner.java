/*
 * Copyright (c) 2018 General Electric Company. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * General Electric Company. The software may be used and/or copied only
 * with the written permission of General Electric Company or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 */
package com.ge.apg.docker;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;

import com.spotify.docker.client.exceptions.DockerException;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.spotify.docker.client.DockerClient;
import com.spotify.docker.client.DockerClient.ListImagesFilterParam;
import com.spotify.docker.client.DockerClient.LogsParam;
import com.spotify.docker.client.DockerClient.RemoveContainerParam;
import com.spotify.docker.client.exceptions.ContainerNotFoundException;
import com.spotify.docker.client.messages.ContainerConfig;
import com.spotify.docker.client.messages.ContainerCreation;
import com.spotify.docker.client.messages.ContainerExit;
import com.spotify.docker.client.messages.HostConfig;
import com.spotify.docker.client.messages.HostConfig.Bind;
import com.spotify.docker.client.messages.Image;

/**
 * Created by Tim Tan 212675067
 */
public class DockerRunner {
    private static final Logger logger = LoggerFactory.getLogger(DockerRunner.class);
    private DockerClient dockerClient;

    public DockerRunner(final DockerClient dockerClient) {
        this.dockerClient = dockerClient;
    }

    /**
     * Returns the image corresponding to the input repository and tag. If multiple images match the
     * query params, it'll throw a duplicate match exception.
     * 
     * @param repository
     *            The repository for the image.
     * @param tag
     *            The specific tag to retrieve.
     * @return An optional returning 0 or 1 found matches.
     */
    public Image getImageByReference(String repository, String tag) throws DockerException, InterruptedException, DockerClientException {
        Validate.notEmpty(repository);
        Validate.notEmpty(tag);
        ListImagesFilterParam param = new ListImagesFilterParam("reference",
                repository + ":" + tag);
        // TODO: Investigate why Image repoTags are considered a list object for a single image.
        List<Image> candidates = dockerClient.listImages(param);
        if (candidates == null || candidates.size() == 0) {
            throw DockerClientException.IMAGE_NOT_FOUND(repository, tag);
        } else if (candidates.size() > 1) {
            throw DockerClientException.TOO_MANY_IMAGES(repository, tag);
        } else {
            return candidates.get(0);
        }
    }

    public CompletableFuture<TaskResultStatus> runTaskAsync(TaskRunOptions runOpts) {
        return CompletableFuture.supplyAsync(() -> {
            try {
                return runTask(runOpts);
            } catch (DockerClientException | DockerException | InterruptedException e) {
                return TaskResultStatus.FAILURE;
            }
        });
    }

    public TaskResultStatus runTask(TaskRunOptions runOpts) throws DockerException, InterruptedException, DockerClientException {
        logger.info("Running task " + runOpts);
        Image image = getImageByReference(runOpts.getImageRepository(), runOpts.getImageTag());
        ContainerConfig config = generateContainerConfig(image, runOpts);
        ContainerCreation createResult = SneakyThrow
                .uncheck(() -> dockerClient.createContainer(config));
        handleWarnings(image, createResult);

        String containerId = createResult.id();

        try {
            dockerClient.startContainer(containerId);

            // long running command.
            ContainerExit containerExit = dockerClient.waitContainer(containerId);
            if (containerExit.statusCode() == 0) {
                return TaskResultStatus.SUCCESS;
            } else {
                logger.error(String.format(
                        "Container [%s] \nfor image [%s]\nfailed.\n STDOUT/STDERR as follows:\n [%s]",
                        containerId, image,
                        dockerClient
                                .logs(containerId, LogsParam.stdout(), LogsParam.stderr())
                                .readFully()));

                return TaskResultStatus.FAILURE;
            }
        } finally {
            logger.info("Execution is finished for container " + containerId);
            try {
                // always remove the container when the process is complete.
                dockerClient.removeContainer(containerId, RemoveContainerParam.forceKill());
            } catch (ContainerNotFoundException e) {
                // do nothing. container already removed.
            }
        }
    }

    private void handleWarnings(Image image, ContainerCreation createResult) {
        List<String> warnings = createResult.warnings();
        if (warnings != null && warnings.size() > 0) {
            logger.warn(String.format("Container [%s] for image [%s] created with warnings [%s]",
                    createResult.id(), image, StringUtils.join(warnings, ',')));
        }

    }

    private ContainerConfig generateContainerConfig(Image image, TaskRunOptions runOpts) {
        String imageReference = StreamUtils.emptyIfNull(image.repoTags()).findFirst().orElse(null);
        Validate.notEmpty(imageReference);

        List<String> environmentVars = new ArrayList<>();
        environmentVars.add(String.format("INPUT_DIRECTORY=%s", "/input"));
        environmentVars.add(String.format("OUTPUT_DIRECTORY=%s", "/output"));

        // TODO generate bindings.
        Bind inputBind = Bind.builder().from(runOpts.getInputFilePath()).to("/input").build();
        Bind outputBind = Bind.builder().from(runOpts.getOutputFilePath()).to("/output").build();

        HostConfig hostConfig = HostConfig.builder().appendBinds(inputBind, outputBind).build();

        return ContainerConfig.builder().image(imageReference).env(environmentVars)
                .hostConfig(hostConfig).build();
    }
}
