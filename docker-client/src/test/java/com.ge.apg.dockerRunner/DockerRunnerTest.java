/*
 * Copyright (c) 2018 General Electric Company. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * General Electric Company. The software may be used and/or copied only
 * with the written permission of General Electric Company or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 */
package com.ge.apg.dockerRunner;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;

import com.ge.apg.docker.DockerClientException;
import com.ge.apg.docker.DockerRunner;
import com.ge.apg.docker.TaskResultStatus;
import com.ge.apg.docker.TaskRunOptions;
import org.apache.log4j.spi.LoggingEvent;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import com.spotify.docker.client.DockerClient;
import com.spotify.docker.client.messages.ContainerCreation;
import com.spotify.docker.client.messages.ContainerExit;
import com.spotify.docker.client.messages.Image;
import com.spotify.docker.client.shaded.com.google.common.collect.ImmutableList;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(loader = AnnotationConfigContextLoader.class)
public class DockerRunnerTest {

    @Mock
    DockerClient dockerClient;

    DockerRunner dockerRunner;

    @Mock
    Image image;

    @Mock
    Image image2;

    @Mock
    ContainerCreation containerCreation;

    @Mock
    ContainerExit containerExit;

    @Before
    public void setup() throws Exception {
        dockerRunner = new DockerRunner(dockerClient);
    }

    @Test
    public void test_getImageByReference_success() throws Exception {
        when(dockerClient.listImages(any())).thenReturn(ImmutableList.of(image));
        Image result = dockerRunner.getImageByReference("demo", "latest");
        Assert.assertEquals(image, result);
    }

    @Test(expected = DockerClientException.class)
    public void test_getImageByReference_noResults() throws Exception {
        when(dockerClient.listImages(any())).thenReturn(null);
        dockerRunner.getImageByReference("demo", "latest");
    }

    @Test(expected = DockerClientException.class)
    public void test_getImageByReference_multipleResults() throws Exception {
        ArrayList<Image> images = new ArrayList<>();
        images.add(image2);
        images.add(image);
        when(dockerClient.listImages(any())).thenReturn(images);
        dockerRunner.getImageByReference("demo", "latest");
    }

    @Test(expected = DockerClientException.class)
    public void test_docker_execute_empty() throws Exception {
        String imageRepository = "image";
        String imageTag = "tag";
        String inputFilePath = "input";
        String outputFilePath = "output";
        when(dockerClient.listImages(any())).thenReturn(null);
        TaskRunOptions options = new TaskRunOptions(imageRepository, imageTag, inputFilePath, outputFilePath);
        dockerRunner.runTask(options);
    }

    @Test
    public void test_docker_execute_success() throws Exception {
        when(image.repoTags()).thenReturn(ImmutableList.of("demo"));
        when(dockerClient.createContainer(any())).thenReturn(containerCreation);
        when(dockerClient.waitContainer(any())).thenReturn(containerExit);
        String imageRepository = "image";
        String imageTag = "tag";
        String inputFilePath = "input";
        String outputFilePath = "output";
        when(dockerClient.listImages(any())).thenReturn(ImmutableList.of(image));
        TaskRunOptions options = new TaskRunOptions(imageRepository, imageTag, inputFilePath, outputFilePath);
        TaskResultStatus runResult = dockerRunner.runTask(options);
        Assert.assertEquals(runResult.toString(), "SUCCESS");
    }
}
