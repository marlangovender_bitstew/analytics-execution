#!/bin/bash

set -e

docker build -f Dockerfile.build -t appengine/build-analytics .
docker build -f Dockerfile.run -t appengine/run-analytics .
