docker run \
--mount type=bind,source=$PWD/input,target=/input \
--mount type=bind,source=$PWD/output,target=/output \
-e INPUT_DIRECTORY='/input' \
-e OUTPUT_DIRECTORY='/output' \
appengine/dummy_container:latest
