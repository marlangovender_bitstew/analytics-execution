#!/bin/bash

set -e

if [[ -z "$INPUT_DIRECTORY" ]]; then
    echo "INPUT_DIRECTORY environment variable is missing"
    exit 1
fi

if [[ -z "$OUTPUT_DIRECTORY" ]]; then
  echo "OUTPUT_DIRECTORY environment variable is missing"
  exit 1
fi

python /scripts/tagmapping_snippet_example.py

echo "Execution was successful"
