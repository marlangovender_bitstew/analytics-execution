'''
This is an example of modules used in tag mapping with csv input and outputs'
'''

import os
import re
import pandas as pd
from sklearn.metrics.pairwise import cosine_similarity
from sklearn.feature_extraction.text import CountVectorizer

def customTokenizer(text):
    '''This function tokenizes the input text
    Args:
        text -- string, the text we wish to tokenize
    Returns:
    token -- a list of tokens

    '''
    #Split the text based on delimiters
    res1 = re.split(' |_|,|\.|-', text)
    #Split the numeric and string parts, for example vib2 to [vib , 2]
    tokens = []
    for s in res1:
        res2 = filter(None,re.split(r'(\d+)', s))
        for token in res2:
            #remove the leading zeros in numeric tokens
            token = token.lstrip('0')
            #remove parenthesis
            token = token.strip('()')
            #Append to the list
            tokens.append(token)
    return(tokens)


def algorithm(inputDataFrame, **kwargs):
    '''Calculates the similarity matrix between two sets of documents
    Args:
        inputDataFrame -- a pandas dataframe which contained the combination of both master and customer tag lists
    Kwargs:
        ngramSize -- indicates the ngram size, the default value is chosen to be 1.
        masterListSize -- indicates that the first masterListSize belongs to master set and the rest to the customer set. 
                          default is set to 100.

    Returns:
        similarityDf --  a pandas dataframe containing the similarity matrix between master and customer documents.

    '''
    if kwargs.__contains__("ngramSize"):
        ngramSize = kwargs["ngramSize"]
    else:
        ngramSize = 1 #default value

    if kwargs.__contains__("masterListSize"):
        masterListSize = kwargs["masterListSize"]
    else:
        masterListSize = 100 # default value

    #Calculate the document-term frequency using the sklearn library
    vectorizer = CountVectorizer(tokenizer=customTokenizer, ngram_range=(1, ngramSize))
    document_term_matrix= vectorizer.fit_transform(inputDataFrame)

    #Split the matrix into master and customer sets:
    master_document_term_matrix = document_term_matrix[:masterListSize]
    customer_document_term_matrix = document_term_matrix[masterListSize:]

    # Calculate the similarity matrix between the two sets based on the cosine similarity
    similarity = cosine_similarity(master_document_term_matrix, customer_document_term_matrix)
    similarityDf = pd.DataFrame(similarity)

    return similarityDf

if __name__ == "__main__":
    inputDir = os.environ['INPUT_DIRECTORY']
    outputDir = os.environ['OUTPUT_DIRECTORY']
    #Retrieves the dataframe from a csv
    inputDataFrame = pd.Series.from_csv(inputDir + '/snippet_input.csv')
    #Set the parameters in a dictionary
    parameters = {"ngramSize" : 2, "masterListSize" : 100}
    #Apply the algorithm
    similarityDataFrame = algorithm(inputDataFrame, **parameters)
    #Store the result in a csv file
    similarityDataFrame.to_csv(outputDir + '/snippet_output.csv')
