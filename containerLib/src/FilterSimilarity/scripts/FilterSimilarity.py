'''
This is an example for filtering the similarity scores
'''

import pandas as pd
import os


def filterSimilarity(similarityDf, threshold = 0.2):
    '''Filters the similarity scores, if the score is lower than threshhold, the score is replaced by zero
    Args:
        similarityDf -- a pandas dataframe which containes the similarity scores
    Returns:
        filteredSimilarityDf --  a pandas dataframe containing the filtered similarity scores
    '''
    filteredSimilarityDf = similarityDf.where(similarityDf>threshold, 0)

    return filteredSimilarityDf

if __name__ == "__main__":
    inputDir = os.environ['INPUT_DIRECTORY']
    outputDir = os.environ['OUTPUT_DIRECTORY']
    inputDataFrame = pd.read_csv(inputDir + '/inputDataframe.csv', header=None)
    print (inputDataFrame)
    outputDataFrame = filterSimilarity(inputDataFrame)
    outputDataFrame.to_csv(outputDir + '/output.csv', header=False, index=False)
