/*
 * Copyright (c) 2018 General Electric Company. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * General Electric Company. The software may be used and/or copied only
 * with the written permission of General Electric Company or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 */
package com.ge.apg.analytics.paf;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class PredixAnalyticsFrameworkClient {

    public static final String HEADER_PREDIX_ZONE_ID = "Predix-Zone-Id";

    private RestTemplate restTemplate;

    public PredixAnalyticsFrameworkClient(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public String executeSync(String endpoint, String predixZoneId, String accessToken, String inputDataFrame) throws PafClientException {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.add(HEADER_PREDIX_ZONE_ID, predixZoneId);
        headers.add(HttpHeaders.AUTHORIZATION, "bearer " + accessToken);

        HttpEntity<String> httpEntity = new HttpEntity<>(inputDataFrame, headers);
        ResponseEntity<String> response = restTemplate.exchange(endpoint, HttpMethod.POST, httpEntity, String.class);
        if (response.getStatusCode() != HttpStatus.OK) {
            throw PafClientException.HttpError(response.getStatusCode());
        }
        return response.getBody();
    }
}
