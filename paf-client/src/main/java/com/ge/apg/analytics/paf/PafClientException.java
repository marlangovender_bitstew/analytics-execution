/*
 * Copyright (c) 2018 General Electric Company. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * General Electric Company. The software may be used and/or copied only
 * with the written permission of General Electric Company or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 */
package com.ge.apg.analytics.paf;

import org.springframework.http.HttpStatus;

public class PafClientException extends Exception {

    public static PafClientException HttpError(HttpStatus status) {
        return new PafClientException(status.toString());
    }

    public PafClientException(String message) {
        super(message);
    }
}
