/*
 * Copyright (c) 2018 General Electric Company. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * General Electric Company. The software may be used and/or copied only
 * with the written permission of General Electric Company or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 */
package com.ge.apg.analytics.paf;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.util.List;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class PredixAnalyticsFrameworkClientTest {

    private RestTemplate mockRestTemplate;
    private PredixAnalyticsFrameworkClient client;

    private String endpoint;
    private String predixZoneId;
    private String accessToken;
    private String inputDataFrame;
    private String mockResponse;

    @Before
    public void setup() throws Exception {
        mockRestTemplate = mock(RestTemplate.class);
        client = new PredixAnalyticsFrameworkClient(mockRestTemplate);
        endpoint = "https://mock/execution";
        predixZoneId = "zone123";
        accessToken = "access-granted";
        inputDataFrame = "hello world";
        mockResponse = "welcome to paf";
    }

    @Test
    public void testExecute() throws Exception {
        when(mockRestTemplate.exchange(anyString(), any(HttpMethod.class), any(HttpEntity.class), any(Class.class)))
                .thenReturn(new ResponseEntity<>(mockResponse, HttpStatus.OK));
        String response = client.executeSync(endpoint, predixZoneId, accessToken, inputDataFrame);
        ArgumentCaptor<HttpEntity> requestCaptor = ArgumentCaptor.forClass(HttpEntity.class);
        verify(mockRestTemplate).exchange(eq(endpoint), eq(HttpMethod.POST), requestCaptor.capture(), eq(String.class));

        Assert.assertNotNull(response);
        Assert.assertEquals("response is not the same", mockResponse, response);
        HttpEntity request = requestCaptor.getValue();
        Assert.assertEquals("request is not the same", inputDataFrame, request.getBody());
        HttpHeaders headers = request.getHeaders();
        List<String> values = headers.get(HttpHeaders.AUTHORIZATION);
        Assert.assertTrue("authorization header is not present", values.size() == 1);
        Assert.assertEquals("authorization value is different", "bearer " + accessToken, values.get(0));
        values = headers.get(HttpHeaders.CONTENT_TYPE);
        Assert.assertTrue("content-type header is not present", values.size() == 1);
        Assert.assertEquals("content-type value is different", MediaType.APPLICATION_JSON_VALUE, values.get(0));
        values = headers.get(PredixAnalyticsFrameworkClient.HEADER_PREDIX_ZONE_ID);
        Assert.assertTrue("predix-zone-id header is not present", values.size() == 1);
        Assert.assertEquals("predix-zone-id value is different", predixZoneId, values.get(0));
    }

    @Test(expected = PafClientException.class)
    public void testHttpStatus() throws Exception {
        when(mockRestTemplate.exchange(anyString(), any(HttpMethod.class), any(HttpEntity.class), any(Class.class)))
                .thenReturn(new ResponseEntity<>(mockResponse, HttpStatus.INTERNAL_SERVER_ERROR));
        client.executeSync(endpoint, predixZoneId, accessToken, inputDataFrame);
    }
}
